package com.vatsa.todolistapp.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TaskDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "task.db";
    private static final int DATABASE_VERSION = 1 ;
    public static final String TABLE_NAME = "Tasks";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CATEG0RY = "name";
    public static final String COLUMN_TASK_NAME = "";
    public static final boolean COLUMN_CHECKED = false;

    public TaskDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CATEG0RY + " TEXT NOT NULL, " +
                COLUMN_TASK_NAME + " NUMBER NOT NULL, " +
                COLUMN_CHECKED + " TEXT NOT NULL);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        this.onCreate(db);
    }
}
