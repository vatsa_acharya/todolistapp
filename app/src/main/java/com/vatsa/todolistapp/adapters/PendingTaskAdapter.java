package com.vatsa.todolistapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;

import com.vatsa.todolistapp.R;
import com.vatsa.todolistapp.dataModels.PendingModel;

import java.util.ArrayList;


public class PendingTaskAdapter extends RecyclerView.Adapter<PendingTaskAdapter.View_Holder> {
    Context mContext;

    public final String TAG = "PendingTaskAdapter";

    ArrayList<PendingModel> list = new ArrayList<>(1);
    PendingModel pendingModel;
    public int mPosition;

    public PendingTaskAdapter(Context applicationContext, ArrayList<PendingModel> pendingList) {
        this.mContext = applicationContext;
        this.list = pendingList;
    }

    @NonNull
    @Override
    public PendingTaskAdapter.View_Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pending_task_item, parent, false);

        return new View_Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PendingTaskAdapter.View_Holder holder, final int position) {
        mPosition = position;
        pendingModel = list.get(position);
//        holder.cb_state.setChecked(pendingModel.isChecked());
//        holder.et_content.setText(pendingModel.getContent_value());
        holder.ib_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.remove(position);
                notifyItemRemoved(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        Log.e(TAG, "getItemCount: "+ list.size());
        return list.size();
    }

    public void addItem(){
        list.add(list.size(),pendingModel);
        notifyItemInserted(list.size());
    }

    public class View_Holder extends RecyclerView.ViewHolder {
        EditText et_content;
        CheckBox cb_state;
        ImageButton ib_cancel;
        public View_Holder(View itemView) {
            super(itemView);
            et_content =itemView.findViewById(R.id.et_content);
            cb_state = itemView.findViewById(R.id.cb_pending);
            ib_cancel = itemView.findViewById(R.id.ib_cancel);
        }
    }
}
