package com.vatsa.todolistapp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vatsa.todolistapp.R;

public class CompletedTasksAdapter extends RecyclerView.Adapter<CompletedTasksAdapter.View_Holder> {
    @NonNull
    @Override
    public CompletedTasksAdapter.View_Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.completed_task_item, parent, false);

        return new View_Holder(itemView);    }

    @Override
    public void onBindViewHolder(@NonNull CompletedTasksAdapter.View_Holder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class View_Holder extends RecyclerView.ViewHolder {
        public View_Holder(View itemView) {
            super(itemView);
        }
    }
}
