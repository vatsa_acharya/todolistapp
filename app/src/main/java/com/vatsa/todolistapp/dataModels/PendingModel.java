package com.vatsa.todolistapp.dataModels;

public class PendingModel {

    boolean isChecked = false;
    String content_value;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getContent_value() {
        return content_value;
    }

    public void setContent_value(String content_value) {
        this.content_value = content_value;
    }




}
