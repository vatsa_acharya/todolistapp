package com.vatsa.todolistapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.vatsa.todolistapp.R;
import com.vatsa.todolistapp.adapters.PendingTaskAdapter;
import com.vatsa.todolistapp.dataModels.PendingModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    PendingTaskAdapter pendingTaskAdapter;

    ArrayList<PendingModel> pendingList;

    PendingModel pendingModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        custom_toolbar();

        pending_tasks();

        completed_tasks();

        add_new_item_view();

    }

    private void add_new_item_view() {
        TextView tv_add_item = findViewById(R.id.tv_add_item);
        tv_add_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pendingTaskAdapter.addItem();

            }
        });
    }


    private void completed_tasks() {

    }

    private void pending_tasks() {
        pendingList = new ArrayList<>(1);
        RecyclerView rv_pending = findViewById(R.id.rv_pending);
        pendingTaskAdapter = new PendingTaskAdapter(getApplicationContext(), pendingList);
        rv_pending.setAdapter(pendingTaskAdapter);
        rv_pending.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    private void custom_toolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final AutoCompleteTextView actv_cat = toolbar.findViewById(R.id.actv_category);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                getApplicationContext(), android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.categoryList));

        actv_cat.setAdapter(arrayAdapter);
        actv_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actv_cat.showDropDown();
            }
        });


    }
}
